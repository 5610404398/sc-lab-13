package Q2;

public class RemoveRunnable implements Runnable{
	private Contact con;
	private int count;
	private String n;
	private static int DELAY = 1000;
	
	public RemoveRunnable(Contact con,int count,String n){
		this.con = con;
		this.count = count;
		this.n = n;
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			for(int i=1;i<=count;i++){
				con.remove(n);
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
			System.err.println("Error");
		}
	}
	
	
}
