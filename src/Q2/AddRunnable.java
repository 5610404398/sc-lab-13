package Q2;

public class AddRunnable implements Runnable{
	private Contact con;
	private int count;
	private String strings;
	private static int DELAY = 1000;
	public AddRunnable(Contact con,int count,String s) {
		this.con = con;
		this.count = count;
		this.strings = s;
		
	}
	
	@Override
	public void run() {
		try{
			for(int i=1;i<=count;i++){
				con.add(strings);
				Thread.sleep(DELAY);
			}
		}catch(InterruptedException e){
			System.err.println("Error");
		}
	}

}
