package Q1;

public class AddRunnable implements Runnable{
	private int count;
	private Management manage;
	
	public AddRunnable(Management manage,int count) {
		this.manage = manage;
		this.count = count;
		
	}
	
	@Override
	public void run() {
		try{
			for(int i=1;i<=count;i++){
				manage.add();
				Thread.sleep(1000);
			}
		}catch(InterruptedException e){
		}
	}

}
